package ide.fxml.edit;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.browser.Browser;

public class PreferencesPage extends Composite{
	private Text txt_editor_location;
	private Text txt_config_file_path;

	public PreferencesPage(Composite parent) {
		super(parent, SWT.NONE);
		GridLayout gridLayout = new GridLayout(3, false);
		gridLayout.marginTop = 5;
		gridLayout.marginRight = 5;
		gridLayout.marginLeft = 5;
		gridLayout.marginBottom = 15;
		gridLayout.verticalSpacing = 15;
		setLayout(gridLayout);
		
		CLabel lblNewLabel = new CLabel(this, SWT.NONE);
		lblNewLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblNewLabel.setText("Editor Location:");
		
		txt_editor_location = new Text(this, SWT.BORDER);
		txt_editor_location.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		Button btn_browse_editor_location = new Button(this, SWT.NONE);
		btn_browse_editor_location.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				FileDialog dialog = new FileDialog(getShell());
				dialog.setFilterExtensions(new String[] {"*.jar", "*.*"});
				dialog.setFilterNames(new String[] {".jar files", "All files"});
				String path = dialog.open();
				if(path != null) {
					EditorLocationInput().setText(path);
				}
			}
		});
		btn_browse_editor_location.setImage(new Image(null, getClass().getResourceAsStream("/ide/fxml/edit/folder.png")));
		
		CLabel lblNewLabel_1 = new CLabel(this, SWT.NONE);
		lblNewLabel_1.setText("Config File Path:");
		
		txt_config_file_path = new Text(this, SWT.BORDER);
		txt_config_file_path.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		Button btn_browse_config_file_path = new Button(this, SWT.NONE);
		btn_browse_config_file_path.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				FileDialog dialog = new FileDialog(getShell());
				dialog.setFilterExtensions(new String[] {"*.conf", "*.config", "*.json", "*.*"});
				dialog.setFilterNames(new String[] {".conf files", ".config files", ".json files", "All files"});
				String path = dialog.open();
				if(path != null) {
					ConfigFilePath().setText(path);
				}
			}
		});
		btn_browse_config_file_path.setImage(new Image(null, getClass().getResourceAsStream("/ide/fxml/edit/folder.png")));
		
		Label label = new Label(this, SWT.SEPARATOR | SWT.HORIZONTAL);
		label.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 3, 1));
		
		Browser browser = new Browser(this, SWT.BORDER);
		browser.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
		browser.setText("<p><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">The <em>FXML editor .jar executable</em> and the <em>project JavaFx configuration file</em> can either be located using absolute file-system paths -or- programmatically generated paths aided by the following tags:</span></p><p><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\"><strong>#ECLIPSE_DIR#</strong> -- Returns the Eclipse installation directory .</span></p><p><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\"><strong>#WORKSPACE_DIR#</strong> -- Returns the projects workspace directory.</span></p><p><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\"><strong>#PROJECT_DIR#</strong> -- Returns the currently active project directory.</span></p><p><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\"><strong>#PLUGIN_DIR#</strong> -- Returns the installation directory of the ide.fxml.edit plugin (used by default for locating the intgrated FXML editor).</span></p>");
	}

	public Text EditorLocationInput() {
		return txt_editor_location;
	}
	
	public Text ConfigFilePath() {
		return txt_config_file_path;
	}
}
