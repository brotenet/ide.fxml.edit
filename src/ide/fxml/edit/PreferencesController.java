package ide.fxml.edit;

import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.osgi.service.prefs.Preferences;

public class PreferencesController extends PreferencePage implements IWorkbenchPreferencePage{

	public static String editor_location = "editor_location";
	public static String editor_location_default = "#ECLIPSE_DIR#fxmleditor.jar";
	public static String config_file_path = "config_file_path";
	public static String config_file_path_default = "#PROJECT_DIR#jfx.conf";
	public static String use_file_prefix = "use_file_prefix";
	public static boolean use_file_prefix_default = true;
	
	
	Preferences preferences = InstanceScope.INSTANCE.getNode(Activator.PLUGIN_ID);
	PreferencesPage page;
	
	@Override
	protected Control createContents(Composite parent) {
		page = new PreferencesPage(parent);
		page.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		page.EditorLocationInput().setText(preferences.get(editor_location, editor_location_default));
		page.ConfigFilePath().setText(preferences.get(config_file_path, config_file_path_default));
		return null;
	}

	@Override
	public void init(IWorkbench workbench) {
		
	}
	
	@Override
	protected void performDefaults() {
		page.EditorLocationInput().setText(editor_location_default);
		page.ConfigFilePath().setText(config_file_path_default);
	}
	
	@Override
	protected void performApply() {
		preferences.put(editor_location, page.EditorLocationInput().getText().trim());
		preferences.put(config_file_path, page.ConfigFilePath().getText().trim());
	}
	
	@Override
	public boolean performOk() {
		performApply();
		return super.performOk();
	}

}
