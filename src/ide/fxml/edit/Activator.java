package ide.fxml.edit;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;


//TODO: Do something like : "java -cp /opt/GIT/jfx-ext/jar/api.media-0.01-Alpha.jar:/opt/GIT/jfx-ext/jar/api.reporting.jasper-0.01-Alpha.jar:/home/user/Public/fxml_editor.jar ide.fxml.editor.FxmlEditor --file=/opt/GIT/jfx-ext/src/tests/TestForm02.fxml --load-extensions=/opt/GIT/jfx-ext/jfx.conf"
//either in here using the .jar editor or directly at the boot script of the AppImage editor

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "ide.fxml.edit"; //$NON-NLS-1$

	// The shared instance
	private static Activator plugin;
	
	/**
	 * The constructor
	 */
	public Activator() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}
	
	public static IProject getSelectedProject() {
		try {
			IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		    if (window != null)
		    {
		        IStructuredSelection selection = (IStructuredSelection) window.getSelectionService().getSelection();
		        Object firstElement = selection.getFirstElement();
		        if (firstElement instanceof IResource)
		        {
		        	return ((IResource) selection.getFirstElement()).getProject(); //(IProject)((IAdaptable)firstElement).getAdapter(IProject.class);
		        }else {
		        	return null;
		        }
		    }else {
		    	return null;
		    }
		} catch (Exception exception) {
			return null;
		}
	}
	
	public static String getSelectedProjectName() {
		try {
			return getSelectedProject().getName();
		} catch (Exception exception) {
			return "";
		}
	}
	
	public static String getProjectPath() {
		try {
			return String.valueOf(getSelectedProject().getLocation()) + System.getProperty("file.separator");
		} catch (Exception exception) {
			return "";
		}		
	}
	
	public static String getPlatformPath() {
		return Platform.getInstallLocation().getURL().getPath();
	}
	
	public static String getWorkspacePath() {
		return ResourcesPlugin.getWorkspace().getRoot().getLocation().toOSString() + System.getProperty("file.separator");
	}
	
	public static String getPluginPath() {
		return Activator.class.getProtectionDomain().getCodeSource().getLocation().getPath();
	}
	
	public static String getString(String resource_path) {
		return getString(Activator.class.getResourceAsStream(resource_path));
	}
	
	public static String getString(InputStream input_stream) {
		BufferedReader buffered_reader = null;
		StringBuilder string_builder = new StringBuilder();
		String line;
		try {
			buffered_reader = new BufferedReader(new InputStreamReader(input_stream));
			while ((line = buffered_reader.readLine()) != null) {
				string_builder.append(line);
			}
		} catch (IOException exception) {
			exception.printStackTrace();
		} finally {
			if (buffered_reader != null) {
				try {
					buffered_reader.close();
				} catch (IOException exception) {
					exception.printStackTrace();
				}
			}
		}
		return string_builder.toString();
	}
	
}
