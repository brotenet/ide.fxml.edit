package ide.fxml.edit;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.ui.IEditorLauncher;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.exceptions.JSONException;
import org.osgi.service.prefs.BackingStoreException;
import org.osgi.service.prefs.Preferences;

public class EditorLauncher implements IEditorLauncher {
	
	IProject project;
	Preferences preferences;
	String platform_path;
	String workspace_path;
	String project_path;
	String plugin_path;
	
	public void open(IPath file_path) {
		File file = file_path.toFile();
		preferences = InstanceScope.INSTANCE.getNode(Activator.PLUGIN_ID);
		project = Activator.getSelectedProject();
		platform_path = Activator.getPlatformPath();
		workspace_path = Activator.getWorkspacePath();
		project_path = Activator.getProjectPath();
		plugin_path = Activator.getPluginPath();
		
		try {
			checkDefaultEditor(platform_path);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		try {
			Thread thread = new Thread(new Runnable() {
				
				@Override
				public void run() {
					try {
						executeCommand(getLauncherCommand(file, preferences, platform_path, workspace_path, project_path, plugin_path));
					} catch (BackingStoreException e) {
						e.printStackTrace();
					}
				}
				
				@Override
				protected void finalize() throws Throwable {
					super.finalize();
					refresh();
				}
			});
			
			thread.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void refresh() {
		try {
			project.refreshLocal(IResource.DEPTH_ZERO, null);
		} catch (CoreException e) {
			
		}
	}
	
	private String executeCommand(String command) {

		StringBuffer output = new StringBuffer();

		Process p;
		try {
			p = Runtime.getRuntime().exec(command);
			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

			String line = "";
			while ((line = reader.readLine()) != null) {
				output.append(line + "\n");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return output.toString();
	}
	
	private static String getLauncherCommand(File file, Preferences preferences, String platform_path, String workspace_path, String project_path, String plugin_path) throws BackingStoreException {
		String output = "java -cp #EDITOR_LOCATION# ide.fxml.editor.Main #FILE_PREFIX##FILE_PATH# #CONFIG_PREFIX##CONFIG_PATH#";
		String editor_location = preferences.get(PreferencesController.editor_location, PreferencesController.editor_location_default).trim();
		String file_prefix = "--file=";
		if(preferences.getBoolean(PreferencesController.use_file_prefix, PreferencesController.use_file_prefix_default) == false) {
			file_prefix = "";
		}
		String file_path = file.getAbsoluteFile().getAbsolutePath();
		String config_prefix = "--load-extensions=";
		String config_path = preferences.get(PreferencesController.config_file_path, PreferencesController.config_file_path_default).trim();
		if(config_path.length() < 1) {
			config_prefix = "";
		}else {
			try {
				JSONArray jars = new JSONObject(new String(Files.readAllBytes(Paths.get(config_path.replace("#ECLIPSE_DIR#", platform_path).replace("#WORKSPACE_DIR#", workspace_path).replace("#PROJECT_DIR#", project_path).replace("#PLUGIN_DIR#", plugin_path))))).getJSONArray("libraries");
				for(String jar : jars.toArray(String.class)) {
					editor_location = editor_location + ":" + (project_path.substring(0, project_path.length()-1)) + jar;
				}
			} catch (JSONException | IOException e) {
				e.printStackTrace();
			}
		}		
		output = output.replace("#EDITOR_LOCATION#", editor_location);
		output = output.replace("#FILE_PREFIX#", file_prefix);
		output = output.replace("#FILE_PATH#", file_path);
		output = output.replace("#CONFIG_PREFIX#", config_prefix);
		output = output.replace("#CONFIG_PATH#", config_path);
		output = output.replace("#ECLIPSE_DIR#", platform_path);
		output = output.replace("#WORKSPACE_DIR#", workspace_path);
		output = output.replace("#PROJECT_DIR#", project_path);
		output = output.replace("#PLUGIN_DIR#", plugin_path);
		return output;
	}
	
	private void checkDefaultEditor(String platform_path) throws IOException{
		if(!new File(platform_path + "fxmleditor.jar").exists()) {
			putDefaultEditor(platform_path);
		}
	}
	
	public void putDefaultEditor(String platform_path) throws IOException {
		File output = new File(platform_path + "fxmleditor.jar");
		output.deleteOnExit();
		FileOutputStream output_stream = new FileOutputStream(output);
		copyInputStreamToOutputStream(getClass().getResourceAsStream("/resources/fxmleditor.jar"), output_stream);
		output_stream.close();
	}
	
	public static OutputStream getOutputStreamFromInputStream(InputStream input_stream) throws IOException {
		OutputStream output_stream = new ByteArrayOutputStream();
		copyInputStreamToOutputStream(input_stream, output_stream);
		return output_stream;
	}
	
	public static void copyInputStreamToOutputStream(InputStream input_stream, OutputStream output_stream) throws IOException {
	    byte[] buffer = new byte[1024];
	    int read;
	    while ((read = input_stream.read(buffer)) != -1) {
	        output_stream.write(buffer, 0, read);
	    }
	}
}